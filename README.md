# Sophos-Student-App

Student App for listing student details with additional feature filter which lists student details based on particular range

#### Prerequisite
- Angular `npm install -g @angular/cli` <br/>
- NodeJS 

#### Steps of Replication on local system:
- git clone `https://gitlab.com/riyaagrahari121/sophos-student-app` <br/>
- Go to created clone folder `cd sophos-student-app` <br/>
- Go to backend folder `cd data-server-backend` and run `npm i` to install all dependencies <br/>
- Run the backend (data server) using `node index` <br/>
- Open new terminal and go to cloned directory. <br/>
- Go to frontend folder `cd student-app-frontend` and run `npm i` to install all dependencies <br/>
- Run the frontend (angular project) using `ng serve` <br/>

#### Outputs
###### Home Page (http://localhost:4200/home)
![Selection_031](/uploads/726a67bed426442d5bdcf3816886f3cc/Selection_031.png)

###### Filtered Students Page (http://localhost:4200/range)
![ss2](/uploads/644675b5d1babd0f4f1c610d1b1b7dec/ss2.png)