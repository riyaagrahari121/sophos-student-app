const Express = require('express');
const BodyParser = require('body-parser');
const app = Express();
const port = 5000;
const cors = require('cors');
const fs = require('fs');
const { request } = require('http');
const dataPath = './data/students_score_card.json';

app.use(cors())
app.use(BodyParser.json());
app.use(BodyParser.urlencoded({extended: true}));

app.listen(port, () => { 
    console.log(`Server started at port ${port}!`)
})

app.get("/api/students",(request, response) => {
    fs.readFile(dataPath,"utf-8", (err,data) => {
        if(err)
        {
            throw err;
        }
        response.status(200).send(JSON.parse(data))
    }) 
});

app.get("/api/filterStudents", (request, response) => {
    const from = request.query.from;
    const to = request.query.to;


    fs.readFile(dataPath,"utf-8", (err,data) => {
        if(err)
        {
            throw err;
        }
    const dataFilter = JSON.parse(data).filter((i) => {
        return i['roll_no'] >= from && i['roll_no'] <= to;
    })
    response.status(200).send(dataFilter.sort((a, b) => a.roll_no - b.roll_no));
    }) 
})

