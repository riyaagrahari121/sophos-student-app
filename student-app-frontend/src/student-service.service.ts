import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http'

export interface Student 
{
    "roll_no": number,
    "name": string,
    "physics_marks": number,
    "chemistry_marks": number,
    "maths_marks": number,
    "result": string
}

@Injectable({
  providedIn: "root"
})
export class StudentService {
  constructor(private http: HttpClient) {}

  getAllStudents(): Observable<Student[]>{
    return this.http.get<Student[]>('http://localhost:5000/api/students')
  }
  getFilterStudents(from, to): Observable<Student[]>{
    return this.http.get<Student[]>(`http://localhost:5000/api/filterStudents?from=${from}&to=${to}`)
  }
}

