import { Component, OnInit } from '@angular/core';
import { StudentService, Student } from '../student-service.service'
@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  students: Student[];
  title = 'students-app';

  header_column = ["Roll No", "Name", "Physics", "Chemistry", "Maths", "Result"];
  headers = ["roll_no", "name", "physics_marks", "chemistry_marks", "maths_marks", "result"];
  constructor(private studentService: StudentService) {
    this.students = [];
  }
  ngOnInit()
  {
  this.studentService.getAllStudents().subscribe((students: Student[]) => {

      this.students = students;
  });
  }
}
  
