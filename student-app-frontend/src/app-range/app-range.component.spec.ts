import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppRangeComponent } from './app-range.component';

describe('AppRangeComponent', () => {
  let component: AppRangeComponent;
  let fixture: ComponentFixture<AppRangeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppRangeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppRangeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
