import { Component, OnInit } from '@angular/core';
import { StudentService, Student } from '../student-service.service'
import {NgForm} from '@angular/forms';
@Component({
  selector: 'app-range',
  templateUrl: './app-range.component.html',
  styleUrls: ['./app-range.component.css']
})
export class AppRangeComponent {
  students: Student[];
  title = 'students-app';

  header_column = ["Roll No", "Name", "Physics", "Chemistry", "Maths", "Result"];
  headers = ["roll_no", "name", "physics_marks", "chemistry_marks", "maths_marks", "result"];

  from : number
  to : number

  onSubmit(form: NgForm) {
    this.from = form.value['from'];
    this.to = form.value['to'];
    this.ngOnInit()
  }  

  constructor(private studentService: StudentService) {
    this.students = [];
  }
  ngOnInit()
  {
  this.studentService.getFilterStudents(this.from, this.to).subscribe((students: Student[]) => {

      this.students = students;
      console.log(this.students)
  });
  }
}
